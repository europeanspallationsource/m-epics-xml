# File to download and unpack.
TARBALL=ftp://xmlsoft.org/libxml2/libxml2-2.9.2.tar.gz
# Path where source tarball was unpacked.
SRCPATH=libxml2-2.9.2

PACKAGE_CONFIGURE_FLAGS="--with-pic --without-python --without-zlib"
