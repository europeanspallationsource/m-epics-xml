LIBOBJS += $(wildcard ../../O.${T_A}/.libs/*.o)

include ${EPICS_ENV_PATH}/module.Makefile

HEADERS_PREFIX = libxml/
SRCPATH=libxml2-2.9.2

SOURCES = -none-

HEADERS += ${SRCPATH}/include/libxml/xmlIO.h
HEADERS += ${SRCPATH}/include/libxml/list.h
HEADERS += ${SRCPATH}/include/libxml/xmlunicode.h
HEADERS += ${SRCPATH}/include/libxml/DOCBparser.h
HEADERS += ${SRCPATH}/include/libxml/parserInternals.h
HEADERS += ${SRCPATH}/include/libxml/entities.h
HEADERS += ${SRCPATH}/include/libxml/xmlautomata.h
HEADERS += ${SRCPATH}/include/libxml/HTMLtree.h
HEADERS += ${SRCPATH}/include/libxml/catalog.h
HEADERS += ${SRCPATH}/include/libxml/HTMLparser.h
HEADERS += ${SRCPATH}/include/libxml/relaxng.h
HEADERS += ${SRCPATH}/include/libxml/xmlschemas.h
HEADERS += ${SRCPATH}/include/libxml/xmlversion.h
HEADERS += ${SRCPATH}/include/libxml/parser.h
HEADERS += ${SRCPATH}/include/libxml/SAX.h
HEADERS += ${SRCPATH}/include/libxml/xlink.h
HEADERS += ${SRCPATH}/include/libxml/nanoftp.h
HEADERS += ${SRCPATH}/include/libxml/valid.h
HEADERS += ${SRCPATH}/include/libxml/xmlmodule.h
HEADERS += ${SRCPATH}/include/libxml/globals.h
HEADERS += ${SRCPATH}/include/libxml/xinclude.h
HEADERS += ${SRCPATH}/include/libxml/xpath.h
HEADERS += ${SRCPATH}/include/libxml/encoding.h
HEADERS += ${SRCPATH}/include/libxml/c14n.h
HEADERS += ${SRCPATH}/include/libxml/xmlreader.h
HEADERS += ${SRCPATH}/include/libxml/xmlregexp.h
HEADERS += ${SRCPATH}/include/libxml/threads.h
HEADERS += ${SRCPATH}/include/libxml/xmlexports.h
HEADERS += ${SRCPATH}/include/libxml/nanohttp.h
HEADERS += ${SRCPATH}/include/libxml/schemasInternals.h
HEADERS += ${SRCPATH}/include/libxml/debugXML.h
HEADERS += ${SRCPATH}/include/libxml/xpointer.h
HEADERS += ${SRCPATH}/include/libxml/xmlstring.h
HEADERS += ${SRCPATH}/include/libxml/xmlerror.h
HEADERS += ${SRCPATH}/include/libxml/SAX2.h
HEADERS += ${SRCPATH}/include/libxml/dict.h
HEADERS += ${SRCPATH}/include/libxml/tree.h
HEADERS += ${SRCPATH}/include/libxml/pattern.h
HEADERS += ${SRCPATH}/include/libxml/xmlschemastypes.h
HEADERS += ${SRCPATH}/include/libxml/xpathInternals.h
HEADERS += ${SRCPATH}/include/libxml/xmlmemory.h
HEADERS += ${SRCPATH}/include/libxml/xmlwriter.h
HEADERS += ${SRCPATH}/include/libxml/xmlsave.h
HEADERS += ${SRCPATH}/include/libxml/schematron.h
HEADERS += ${SRCPATH}/include/libxml/hash.h
HEADERS += ${SRCPATH}/include/libxml/uri.h
HEADERS += ${SRCPATH}/include/libxml/chvalid.h
HEADERS += ../../O.${EPICS_HOST_ARCH}/include/libxml/xmlversion.h

vpath %.h ../../O.${EPICS_HOST_ARCH}/include
